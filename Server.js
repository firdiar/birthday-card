const port = process.env.PORT || 3000;; //Specify a port for our web server
const express = require('express'); //load express with the use of requireJs


const app = express(); //Create an instance of the express library
const server = require('http').createServer(app);
const io = require('socket.io')(server);

app.use(express.static(__dirname + '/'));//Serving static files

io.on('connection', client => {
  console.log('client connected ',client.id);
  client.on('event', data => {
	let msg = {data : data , from : client.id}
	io.emit('event', msg);
  });
  client.on('pesanAdmin', data => {
	client.broadcast.emit('pesanAdmin', data);
  });
  client.on('disconnect', () => { 
	console.log('client disconnected ',client.id);
  });
});




server.listen(port, function() { //Listener for specified port
    console.log("Server running at: http://localhost:" + port)
});