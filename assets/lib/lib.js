const gameWidth = 1920;
const gameHeight = 1080;

let reffGame = null

function AddText(x, y, text, fontStyle, fontSize, fill, align){
  return reffGame.add.text(x, y, text, {font: fontStyle + ' ' + fontSize + 'px Century Gothic', fill: fill, align: align});
}

function AddImage(x, y, key){
  return reffGame.add.image(x, y, key);
}

function AddSprite(x, y, key){
  return reffGame.add.sprite(x, y, key);
}

function AddButton(x, y, key, callback, callbackContext){
  var btn = reffGame.add.button(x, y, key, callback, callbackContext,0,0,1);
  btn.anchor.set(0.5);
  //var pops = AddSound('Pops');
  //btn.onInputUp.add(function(){btn.frame = 0; pops.play('high') });
  //btn.onInputDown.add(function(){btn.frame = 1; pops.play('low') });
  return btn;
}

function AddEmitter(x,y, max , sprite,arr , width , disableRotate){
	let emitter = reffGame.add.emitter(x,y, max);
	emitter.makeParticles(sprite, arr);
	if(disableRotate){
		emitter.minRotation = 0;
		emitter.maxRotation = 0;
	}
	
	
	emitter.width = width;
	
	
    
	
	
	﻿
	return emitter
}

function getFrameFromChar(chars){
	//console.log(chars);
	chars = chars.toLowerCase()
	if(chars == 'a'||chars == 'e'||chars == 'h'){
		return 3
	}else if(chars == 'b'||chars == 'm'||chars == 'p'){
		return 1
	}else if(chars == 'c'||chars == 'd'||chars == 'g'||chars == 'n'||chars == 'j'
				||chars == 'k'||chars == 'r'||chars == 's'||chars == 't'||chars == 'x'
				||chars == 'z'){
		return 2
	}else if(chars == 'f'||chars == 'v'||chars == 'y'||chars == 'i'){
		return 4
	}else if(chars == 'o'||chars == 'l'){
		return 5
	}else if(chars == 'q'||chars == 'w'||chars == 'u'){
		return 6
	}else if(chars == '.'){
		return 0
	}else{
		return 1
	}
}

let isPassDate = true

let repeatCount = 2;

function nyalakanLampu(){
	
	
	
	console.log('Menyalakan lampu...');
	if(!isPassDate){
		alert('Belum bisa menyalakan lampu sekarang maaf\nTolong tunggu yaa sampai waktu habis...');
		return;
	}
	
	if(window.innerHeight > window.innerWidth){
		alert('Maaf kamu harus memiringkan layar 90 derajat');
		return;
	}
	if(Phaser.Device.android && repeatCount>0){
		alert('Android terdeteksi, tolong gunakan PC/Laptop\nklik lagi sebanyak '+(repeatCount-1)+" kali untuk tetap membuka" );
		repeatCount--;
		return
	}
	repeatCount = 2;
	reffGame.state.start('mainmenu');
	document.getElementById("02").style.display = 'none'
	document.getElementById("01").style.display = 'none'
	
	
}
adminMode = 0;
function toAdminMode(){
	adminMode++
	if(adminMode == 10){
		adminMode = 0;
		
		askAdmin();
	}
	console.log("onGoingToAdmin")
}

function askAdmin(){
	let pass = prompt("Insert Admin password to unlock it now")
	console.log(pass);
	
	if(pass=='4y4c4ntik'){

		//document.getElementById("switch").disabled = false;
		document.getElementById("status").innerHTML = "Lampu dapat dinyalakan, Selamat ulang tahun Aya...";
		
		alert('you\'re in Admin Mode');
		isPassDate = true;
		
	}else{
		alert('Sorry you\'re not admin of this page, contact admin to get the password');
	}
}

function clamp(val, min, max){
    return Math.max(min, Math.min(max, val));
}

function getImage(name){
  return "assets/img/"+name;
}
function getAudio(name){
  return "assets/audio/"+name;
}

function getJson(name){
  return "assets/data/"+name;
}

function randomInt(min,max){
	return Math.floor(Math.random() * (max - min)) + +min; 
}

//mendapatkan normalize vector
function getNormalizeVector(x,y){
  var length = getLengthVector(x,y); //calculating length
  if(length != 0){
    x = x/length; //assigning new value to x (dividing x by lenght of the vector)
    y = y/length; //assigning new value to y
  }
  return {x:x , y:y};
}

// mendapatkan panjang vector
function getLengthVector(x,y){
  return Math.sqrt(x*x+y*y);
}

// mendapatkan angle dari vector
function getAngle(x, y) {
  return Math.atan2(y, x) * 180 / Math.PI;
}

// fungsi dari rumus analitik untuk mendapatkan posisi objek
function getCalculationPositionAnalitik(time , angle , startSpeed , vectorStart){
  let g = 10;
  return {
           x: vectorStart.x+startSpeed*Math.cos(angle*Math.PI/180)*time ,
           y: vectorStart.y+(1/2)*g*(time*time) + startSpeed*Math.sin(angle* Math.PI/180)*time
         }
}

// fungsi dari rumus numerik untuk mendapatkan posisi objek
function getCalculationPositionNumerik(DeltaTime , angle , startSpeed ,vectorPrev , vectorStart , vy){
    return {
             x : vectorPrev.x + startSpeed *Math.cos(angle* Math.PI/180) * DeltaTime,
             y : vectorPrev.y + (vy*DeltaTime)
           }
}




// convert value
function NormalToGameY(gamey){
    return height-gamey;
}
function NormalToGameX(gamex){
    return gamex;
}
function GameToNormalY(gamey){
    return height-gamey;
}
function GameToNormalX(gamex){
    return gamex;
}
