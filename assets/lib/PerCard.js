function Card1(obj , arr , eventCall,idxEvent , context){
	
	let text = AddText(obj.width/2+10 , 60,  'Happy Birthday', '', '45', '#000000', 'center');
	text.anchor.set(0.5);
	arr.push(text)
	text = AddText(obj.width/2+10 , 120,  'Untuk', '', '27', '#808080', 'center');
	text.anchor.set(0.5);
	arr.push(text);
	text = AddText(obj.width/2+10 , 180,  'Ni Kadek Nandya Candha\nSanti Pratiwi', '', '27', '#808080', 'center');
	text.anchor.set(0.5);
	arr.push(text);
	
	text = AddButton(obj.width/2+10 , 280,'btn' , context.flipRight , context  )
	//console.log(text);
	let tmp = AddText(0 , 0,  'Mulai', 'bold', '29', '#ffffff', 'center');
	tmp.anchor.set(0.5);
	text.addChild(tmp);
	arr.push(text);
	text.inputEnabled = true;
	
	let sprite = context.add.sprite(obj.width/2+10, 0)
	sprite.anchor.set(0.5)
	let particle = AddEmitter(0 ,0, 500 , 'LoveParticle' ,[0,1],obj.width*2/3,true)
	particle.gravity = -10;	 
	particle.maxParticleScale = 1;
    particle.minParticleScale = 0.2;
	particle.setXSpeed(0,0) 
	particle.setYSpeed(-50,-100)
	particle.setAlpha(0 , 1, 1500,  Phaser.Easing.Power1, true);
	sprite.addChild(particle)
	arr.push(sprite);
	
	let card = AddSprite(obj.width/2+10 , -80, 'OpenCard');
	card.anchor.set(0.5)
	card.animations.add('open');
	arr.push(card);
	
	let tween = null;
	let eventLoop = null;
	let music = context.add.audio('hbd');
	music.loop = true;
	music.play();
	music.volume = 1;
	
	
	eventCall[idxEvent] = function(){
		socket.emit('event', 'Client on first page');
		//particle.visible = true
		particle.on = true
		//text.inputEnabled = true;
		card.animations.play('open', 10, false);
		tween = context.add.tween(card).to({ y:-120 }, 1500 , Phaser.Easing.Cubic.Out,true,250);
		//particle.start(false, 3000,250,100,true);
		particle.start(false, 3000,100);
		
		if(eventLoop != null){
			eventLoop.loop = false;
			eventLoop = null;
		}
		
		
		
	}
	eventCall[idxEvent+2] = function(){
		//console.log('card 1 is disable');
		//card.animations.play('open', 10, false);
		if(tween != null){
			tween.stop();
			tween = null;
		}
		//text.inputEnabled = false;
		card.y = -80
		card.animations.stop(null,true)
		particle.on = false
		//particle.visible = false
		
		eventLoop = context.time.events.loop(25,()=>{
			
			//console.log(eventLoop);
			music.volume -= 0.1;
			if(music.volume <= 0){
				music.stop()
				if(eventLoop!=null){
					eventLoop.loop = false;
					eventLoop = null;
				}
			}
		});
		
	}
}


function Card2(obj , arr , eventCall,idxEvent , context){
	
	let text = AddText(obj.width/2 +10, -300,  'Aya’s Birthday Cake', '', '45', '#000000', 'center');
	text.anchor.set(0.5);
	arr.push(text)
	text = AddText(obj.width/2 +10 , 280,  'Semoga aya jadi\norang yang bermanfaat\ninsyaallah', '', '34', '#000000', 'center');
	text.anchor.set(0.5);
	arr.push(text);
	
	
	text = AddImage(obj.width/2+10, -30, 'CakeBg')
	text.anchor.set(0.5);
	arr.push(text);
	
	let sprite = context.add.sprite(obj.width/2+10, -250)
	sprite.anchor.set(0.5)
	let particle = AddEmitter(0 ,0, 500 , 'Sparkle' ,[0,1,2,3,4,5,6,7,8,9,10,11,12],obj.width*2/3,false)
	particle.maxParticleScale = 0.3;
    particle.minParticleScale = 0.2;
	particle.gravity = 10;
	particle.setXSpeed(-30,30)
	particle.setYSpeed(0,10)	
	sprite.addChild(particle)
	
	let mask = context.add.graphics(obj.width*5.1/6+10, -230);
	mask.beginFill(0xffff00);
	mask.drawRect(0,0, obj.width*4.2/6, 400);
	//mask.drawCircle(0,0, 100);
	sprite.mask = mask
	arr.push(sprite);
	arr.push(mask);
	
	
	
	
	text = AddImage(obj.width/2+10, -30, 'Cake')
	text.anchor.set(0.5);
	arr.push(text);
	
	fire = []
	fire[0] = AddSprite(298,-135, 'FireCaddle')

	fire[1] = AddSprite(260,-110, 'FireCaddle')

	fire[2] = AddSprite(344,-110, 'FireCaddle')
	
	fire[3] = AddSprite(360,-130, 'FireCaddle')
	fire[4] = AddSprite(245,-130, 'FireCaddle')

	for(let i=0; i <5; i++){
		fire[i].anchor.set(0.5);
		fire[i].animations.add('burn');
		arr.push(fire[i]);
	}
	
	
	
	let sprite2 = context.add.sprite(obj.width/2+10, -250)
	sprite2.anchor.set(0.5)
	let particle2 = AddEmitter(0 ,0, 500 , 'Sparkle' ,[0,1,2,3,4,5,6,7,8,9,10,11,12],obj.width*2/3,false)
	particle2.maxParticleScale = 0.4;
    particle2.minParticleScale = 0.3;
	particle2.gravity = 20;
	particle2.setXSpeed(-30,30)
	particle2.setYSpeed(10,20)	
	sprite2.addChild(particle2)
	sprite2.mask = mask
	arr.push(sprite2);
	
	
	
	
	eventCall[idxEvent] = function(){
		particle.on = true
		particle2.on = true
		for(let i=0; i <5; i++){
			fire[i].animations.play('burn', randomInt(9,12), true);
		}
		particle.start(false, 10000,200 , 40 , true);
		particle2.start(false, 10000,400 , 20 , true);
		
		particle.start(false, 10000,200);
		particle2.start(false, 10000,400);
		
		
	}
	eventCall[idxEvent+2] = function(){
		//console.log('card 2 is disable');
		for(let i=0; i <5; i++){
			fire[i].animations.stop(null, true);
		}
		particle.on = false
		particle2.on = false
		
	}
}



function Card3(obj , arr , eventCall,idxEvent , context){
	
	
	
	
	
	let sprite = AddImage(obj.width/2+20 , -180,  'DialogBox');
	sprite.anchor.set(0.5)
	arr.push(sprite);
	
	let text = AddText(obj.width/2+10 , -225,  '...', '', '34', '#000000', 'center');
	text.anchor.set(0.5);
	sprite.dialog = text;
	arr.push(text)
	
	let chara = AddSprite(obj.width/2+10 , 160,  'Character');
	chara.anchor.set(0.5)
	arr.push(chara);
	
	let music = context.add.audio('selamat-ulangtahun');
	let music2 = context.add.audio('happy');
	music2.loop = true;
	
	let isMusicPlaying = false;
	context.sound.onMute.add(()=>{
		if(isMusicPlaying){
			console.log('muted');music.pause()
		}
	} , this)
	context.sound.onUnMute.add(()=>{if(isMusicPlaying){console.log('unmuted');music.resume();}} , this)
	music.onStop.addOnce(function() { 
							isMusicPlaying = false
							music2.volume = 1;
							music2.play();
						}, this);
	
	
	let btn = AddButton(obj.width*4.5/6 , 200,'btn' , context.flipRight , context  )
	let tmp = AddText(0 , 0,  'Next', 'bold', '40', '#ffffff', 'center');
	tmp.anchor.set(0.5);
	btn.scale.set(0.6);
	btn.inputEnabled = false;
	btn.addChild(tmp);
	btn.frame = 1;
	arr.push(btn);

	
	
	var json = context.cache.getJSON('card2');
	//console.log(json)
	let tween = null;
	let eventLoop = null;
	

	talker.reset();
	let at = 0;
	//let tween = null;
	
	let eventStartTalker = null;
	eventCall[idxEvent] = function(){
		socket.emit('event', 'Client on sing page');
		music.play();
		music.volume = 0;
		isMusicPlaying = true
		
		
		
		eventLoop = context.time.events.loop(100,()=>{
			
			//console.log(music.volume);
			music.volume += 0.05;
			music2.volume -= 0.05;
			if(music.volume >= 1){
				music.volume = 1;
				music2.volume = 0;
				music2.stop();
				if(eventLoop!=null){
					eventLoop.loop = false;
					eventLoop = null;
					//startTalker(chara,text , json , btn);
				}
			}
		});
		console.log(at , at==0?2310:2580)
		eventStartTalker = context.time.events.add(at==0?2310:2580, ()=>{
			talker.setTextDialog( chara,text , json , ()=>{
				btn.inputEnabled = true;
				btn.frame = 0;
				if(eventStartTalker!=null){
					eventStartTalker = null;
				}
			});
			
		} );
		at++;
		
		
		//music.restart();
		
	}
	eventCall[idxEvent+2] = function(){
		btn.inputEnabled = false;
		if(eventStartTalker!=null){
			
			context.time.events.remove(eventStartTalker);
			eventStartTalker = null;
		}
		talker.reset();
		
	}
}
// function startTalker(chara,text,json , btn){
	// talker.reset();
	// talker.setTextDialog( chara,text , json , ()=>{btn.inputEnabled = true;btn.frame = 0;});
// }



function Card4(obj , arr , eventCall,idxEvent , context){
	

	
	
	let img = AddImage(obj.width/2, -250, 'Tali')
	img.anchor.set(0.5);
	arr.push(img);
	
	let sprite = AddSprite(obj.width/2, -20 , 'Liontin')
	sprite.anchor.set(0.5)
	arr.push(sprite);
	
	img = AddImage(obj.width/2, 300, 'Hope')
	img.anchor.set(0.5);
	arr.push(img);
	
	let text = AddText(0 , 0,  'Semangat Aya', '', '22', '#ffffff', 'center');
	text.anchor.set(0.5);
	img.addChild(text)
	
	var json = context.cache.getJSON('card4');
	
	let isFront = true
	let change = -1
	let ignore = 0
	
	context.time.events.loop(50,()=>{
		if(ignore > 0){
			ignore--
			return
		}
		if(change != -1){
			sprite.frame = change
			change = -1
		}
		
		if(isFront){
			sprite.scale.x = clamp(sprite.scale.x+0.02 , -1 , -0.05);
		}else{
			sprite.scale.x = clamp(sprite.scale.x-0.02 , -1 , -0.05);
		}
		
		if(sprite.scale.x == -0.05 || sprite.scale.x == -1){
			isFront = (isFront==false)
			if(sprite.scale.x == -0.05){
				change = sprite.frame==0?1:0
				sprite.frame = 2
				text.text = json.Motivate[randomInt(0,json.Motivate.length)]
				ignore = 3
			}else if(sprite.scale.x == -1){
				ignore = 2
			}
		}
		
		
	})
	


	
	
	
	eventCall[idxEvent] = function(){

		
	}
	eventCall[idxEvent+2] = function(){

		
	}
}



function Card5(obj , arr , eventCall,idxEvent , context){
	
	
	
	
	
	let sprite = AddImage(obj.width/2+20 , -180,  'DialogBox');
	sprite.anchor.set(0.5)
	arr.push(sprite);
	
	let text = AddText(obj.width/2+10 , -225,  '...', '', '34', '#000000', 'center');
	text.anchor.set(0.5);
	sprite.dialog = text;
	arr.push(text)
	
	let chara = AddSprite(obj.width/2+10 , 160,  'Character');
	chara.anchor.set(0.5)
	arr.push(chara);
	
	
	
	let btn = AddButton(obj.width*4.5/6 , 200,'btn' , context.flipRight , context  )
	let tmp = AddText(0 , 0,  'Next', 'bold', '40', '#ffffff', 'center');
	tmp.anchor.set(0.5);
	btn.scale.set(0.6);
	btn.inputEnabled = false;
	btn.addChild(tmp);
	btn.frame = 1;
	arr.push(btn);

	
	
	var json = context.cache.getJSON('card4');
	//console.log(json)
	let tween = null;
	
	

	
	//let tween = null;
	
	
	eventCall[idxEvent] = function(){
		socket.emit('event', 'Client on message page');
		talker.setTextDialog( chara,text , json.Speak , ()=>{btn.inputEnabled = true;btn.frame = 0;});
		
		//music.restart();
		
	}
	eventCall[idxEvent+2] = function(){
		btn.inputEnabled = false;
		
	}
}


function Card6(obj , arr , eventCall,idxEvent , context){
	

	let text = AddText(obj.width/2+10, -230,  'Mystery Box\nPassword', '', '40', '#000000', 'center');
	text.anchor.set(0.5);
	arr.push(text)
	
	
	img = AddImage(obj.width/2, -30, 'Password')
	img.anchor.set(0.5);
	arr.push(img);

	text = AddText(obj.width/2+10, 180,  'Trimakasih\ntelah menunggu\nhingga tuntas', '', '30', '#000000', 'center');
	text.anchor.set(0.5);
	arr.push(text)
	
	text = AddText(obj.width/2+10, 290,  'tgl 25 bulan 6', 'bold', '34', '#000000', 'center');
	text.anchor.set(0.5);
	arr.push(text)
	
	var json = context.cache.getJSON('card4');



	
	
	
	eventCall[idxEvent] = function(){

		
	}
	eventCall[idxEvent+2] = function(){

		
	}
}

function Card7(obj , arr , eventCall,idxEvent , context){
	
	
	
	
	
	let sprite = AddImage(obj.width/2+20 , -180,  'DialogBox');
	sprite.anchor.set(0.5)
	arr.push(sprite);
	
	let text = AddText(obj.width/2+10 , -225,  '...', '', '34', '#000000', 'center');
	text.anchor.set(0.5);
	sprite.dialog = text;
	arr.push(text)
	
	let chara = AddSprite(obj.width/2+10 , 160,  'Character');
	chara.anchor.set(0.5)
	arr.push(chara);
	
	
	
	let btn = AddButton(obj.width*4.5/6 , 200,'btn' , context.flipRight , context  )
	let tmp = AddText(0 , 0,  'Next', 'bold', '40', '#ffffff', 'center');
	tmp.anchor.set(0.5);
	btn.scale.set(0.6);
	btn.inputEnabled = false;
	btn.addChild(tmp);
	btn.frame = 1;
	arr.push(btn);

	
	
	var json = context.cache.getJSON('card7');
	//console.log(json)
	let tween = null;
	
	

	
	//let tween = null;
	
	
	eventCall[idxEvent] = function(){
		socket.emit('event', 'Client on secret code page');
		talker.setTextDialog( chara,text , json , ()=>{btn.inputEnabled = true;btn.frame = 0;});
		
		//music.restart();
		
	}
	eventCall[idxEvent+2] = function(){
		btn.inputEnabled = false;
		
	}
}



function Card8(obj , arr , eventCall,idxEvent , context){
	
	
	
	
	
	let sprite = AddImage(obj.width/2+20 , -180,  'DialogBox');
	sprite.anchor.set(0.5)
	arr.push(sprite);
	
	
	let text = AddText(0,-50,  '...', '', '34', '#000000', 'center');
	text.anchor.set(0.5);
	sprite.dialog = text;
	sprite.addChild(text);
	
	let chara = AddSprite(obj.width/2+10 , 160,  'Character');
	chara.anchor.set(0.5)
	arr.push(chara);
	
	
	
	
	
	

	
	
	var json = context.cache.getJSON('card8');
	//console.log(json)
	
	let mask = context.add.graphics(obj.width*5.5/6+10, -380);
	mask.beginFill(0xffff00);
	mask.drawRect(0,0, obj.width*5/6, obj.height-100);
	
	
	
	
	let str = ""
	json.Credit.forEach((item)=>{
		 str +="\n\n"+item;
	})
	//credit.text = str;
	let credit = AddText(obj.width/2+10,500,  str, '', '30', '#000000', 'center');
	credit.anchor.set(0.5 , 0);
	arr.push(credit);
	
	credit.mask = mask
	//arr.push(sprite);
	arr.push(mask);
	
	
	let tween = null;

	let tween2 = null;
	
	let eventLoop = null;
	
	
	eventCall[idxEvent] = function(){
		socket.emit('event', 'Client on end page');
		talker.setTextDialog( chara,text , json.Speak , ()=>{
			tween = context.add.tween(chara).to({ alpha:0 }, 1500 , Phaser.Easing.Cubic.Out,true,250);
			tween2 = context.add.tween(sprite).to({ alpha:0 }, 1500 , Phaser.Easing.Cubic.Out,true,250);
			
			eventLoop = context.time.events.loop(25,()=>{
				credit.y = clamp(credit.y-1 , -400-(credit.height) , 500);
				
			});
			socket.emit('event', 'Client finish all card');
			
		});
		
		//music.restart();
		
	}
	eventCall[idxEvent+2] = function(){
		if(tween != null){
			tween.stop();
			tween = null;
		}
		if(tween2 != null){
			tween2.stop();
			tween2 = null;
		}
		
		if(eventLoop != null){
			eventLoop.loop = false;
			eventLoop = null;
		}
		
		credit.y = 500
		
	}
}