Game.MainMenu = function(game){}



Game.MainMenu.prototype = {
	card : [],
	cardFlipping : null,
	cardIdx : -1,
	flipTo : 0,
	nextCardIdx : -1,
	k : null,
	soundEffect : null,
	music : null,
	eventLoop : null,
	create : function(){
		socket.emit('event', 'Client turn on the light');
		console.log(this.stage);
		//this.stage.disableVisibilityChange = true; 
		//this.sound.muteOnPause = false;
		//this.sound.onMute.add(()=>{console.log("mute")} , this)
		//this.sound.onMute.add(()=>{console.log("unmute")} , this)
		this.cardIdx = 0;
		
		for(let i =3; i >= 0; i--){
			this.card[i]=(this.add.image(gameWidth/2,gameHeight/2,'Card'));
			this.card[i].anchor.setTo(0,0.5);
			this.card[i].arrFrontObject = []
			this.card[i].arrBackObject = []
			AddObjectToCard(this.card[i] , i,this);
			AddEventCard(this.card[i]);
			//this.card[i].addChild(text);
		}
		//this.onPause.add(this.onP , this);
		this.soundEffect = this.add.audio('flip');
		this.soundEffect.volume = 0.2
		//this.triggerStartEvent(this.cardIdx);
		//this.flipRight();
		this.k = this.input.keyboard;
		this.setTextDialogLoop()
		
		
		let bg = document.getElementById("blackBG");
		console.log(bg.style.opacity);
		this.eventLoop = reffGame.time.events.loop(100,()=>{
			console.log(bg.style.opacity);
			bg.style.opacity = clamp(bg.style.opacity-0.04,0,1);
			//console.log(bg.style.opacity);
			if(bg.style.opacity==0&&this.eventLoop!=null){
				this.eventLoop.loop = false;
				this.eventLoop = null;
				bg.style.display =  'none'
				this.triggerStartEvent(this.cardIdx);
			}
		})

	},
	flipLeft : function(){//flip left card to right
		//console.log(this.cardFlipping == null,this.cardIdx-1);
		if( this.cardFlipping == null){
			if(this.cardIdx-1 <= -1){
				console.log("cant flip left anymore")
				return;
			}
			this.cardFlipping = this.card[this.cardIdx-1]
			if(this.cardFlipping!=null){
				this.cardFlipping.bringToTop()
				this.soundEffect.play()
			}
		}else{
			return;
		}
		this.nextCardIdx = this.cardIdx-1
		this.flipTo = -1

	},
	flipRight : function(){//flip right card to left
		//console.log(this.cardFlipping == null ,this.cardIdx );
		if( this.cardFlipping == null ){
			if(this.cardIdx >= this.card.length){
				console.log("cant flip right anymore")
				return;
			}
			this.cardFlipping = this.card[this.cardIdx]
			if(this.cardFlipping!=null){
				this.cardFlipping.bringToTop()
				this.soundEffect.play()
			}
		}else{
			return;
		}
		this.nextCardIdx = this.cardIdx+1
		this.flipTo = 1
		
	},
	paused : function(){
		console.log('paused')
	},
	update : function(){
		
		//this.loop++;
		
		if(this.cardFlipping == null){
			this.flipTo = 0
		}else{
			
			this.cardFlipping.onChangeScaleX();
		}
		
		//console.log(card.scale.x);
		if(this.flipTo == -1){
			this.cardFlipping.scale.setTo(this.cardFlipping.scale.x + 0.2 , 1);
		}else if(this.flipTo == 1){
			this.cardFlipping.scale.setTo(this.cardFlipping.scale.x - 0.2 , 1);
		}
		
		if((this.cardFlipping != null)&&(this.cardFlipping.scale.x <= -1 ||this.cardFlipping.scale.x >= 1) ){
			this.flipTo = 0
			//console.log(this.cardFlipping.scale.x);
			this.cardFlipping.scale.setTo( Math.round(this.cardFlipping.scale.x) , 1);
			//console.log(this.cardFlipping.scale.x);
			this.cardFlipping = null;
			this.triggerStopEvent(this.cardIdx);
			
			this.cardIdx = this.nextCardIdx;
			
			this.triggerStartEvent(this.cardIdx);
			socket.emit('event', 'Client at '+this.cardIdx);
		}
		
		
		
		// A
		if (this.k.isDown(65)) {
			this.flipRight();
		}

		// D
		if (this.k.isDown(68)) {
			this.flipLeft();
		}
		
 
		
	},
	setTextDialogLoop(){
		talker.game = this;
		talker.eventLoop = this.time.events.loop(500,()=>talker.update())
	},
	triggerStartEvent(phase){
		let card1 = this.card[this.cardIdx]
		let card2 = this.card[this.cardIdx-1]
		
		
		if(card2!=null && card2.events[1]!=null){
			card2.events[1]()
		}
		if(card1!=null && card1.events[0]!=null){
			card1.events[0]()
		}
	},
	triggerStopEvent(phase){
		let card1 = this.card[this.cardIdx]//front&back side
		let card2 = this.card[this.cardIdx-1]//back side
		let card3 = this.card[this.cardIdx+1]//front side
		
		if(card1!=null && card1.events[2]!=null){
			card1.events[2]()//card now bag front
			//console.log(this.cardIdx,'bag','front');
		}
		if(card1!=null && card1.events[3]!=null){
			card1.events[3]()//card now bag front
			//console.log(this.cardIdx,'bag','back');
		}
		if(card2!=null && card2.events[3]!=null){
			card2.events[3]()
			//console.log(this.cardIdx-1,'bag','back');
		}
		if(card3!=null && card3.events[2]!=null){
			card3.events[2]()
			//console.log(this.cardIdx+1,'bag','front');
		}
	}

}

talker = {
	update(){
		if(!this.chapterFinish){
			if(this.dialogFinish){
				this.nextDialogCalled = true
				this.dialogFinish = false
				console.log("get next trigger");
				if(this.dialogIndex==-1){
					this.getNextDialog()
					return
				}
				this.game.time.events.add(300, ()=>{this.character.frame = 1;} );
				
				this.game.time.events.add(this.arrayDialog[this.dialogIndex].time, ()=>this.getNextDialog() );
				return
			}
			
			if(this.currentDialog != this.targetDialog){
				
				this.currentDialog = this.targetDialog.substring(0, clamp(this.currentDialog.length +1 , 0 , this.targetDialog.length ) );
				this.textBox.text = this.currentDialog;
				//if(this.frameChange == 0 ){
					this.setFrameChar(this.currentDialog.charAt(this.currentDialog.length-1));
					//this.frameChange = 0
				//}
				
			}else if(!this.nextDialogCalled){
				this.dialogFinish = true
			}
			
		}
	},
	chapterFinish : true,
	dialogFinish : false,
	nextDialogCalled : false,
	currentDialog : '',
	targetDialog : '',
	dialogIndex : 0,
	arrayDialog : [],
	textBox : null,
	eventLoop : null,
	game : null,
	character  : null,
	callBack : null,
	setFrameChar(chars){
		if(this.character!=null)
			this.character.frame = getFrameFromChar(chars);
	},
	reset(){
		this.chapterFinish = true
		this.dialogFinish = false
		this.nextDialogCalled = false
		this.currentDialog = ''
		this.targetDialog = ''
		this.dialogIndex = 0
		this.arrayDialog = []
		if(this.textBox!=null){
			this.textBox.text = '...'
		}
		this.textBox = null
		this.callBack = null
		this.setFrameChar('b');
	},
	setTextDialog(chara , textBox , array , callBack ){
		this.dialogIndex = -1;
		this.dialogFinish = true;
		this.currentDialog = ''
		this.targetDialog = ''
		this.arrayDialog = array
		this.textBox = textBox 
		this.character =chara
		this.chapterFinish = false
		this.callBack = callBack
		this.frameChange = 0
	},
	getNextDialog(){
		
		this.nextDialogCalled = false
		console.log("get Next dialog",this.dialogIndex,this.arrayDialog.length);
		this.dialogIndex++
		if(this.dialogIndex >= this.arrayDialog.length){
			if(this.callBack != null){
				this.callBack()
			}
			this.chapterFinish = true
			return
		}
		this.currentDialog = ''
		this.targetDialog = this.arrayDialog[this.dialogIndex].text
		this.eventLoop.delay = this.arrayDialog[this.dialogIndex].speed
		console.log(this.targetDialog);
		this.dialogFinish = false;
		
	}
	
}





function AddObjectToCard(obj , idx , context){
	
	obj.events = []
	switch(idx){
		case 0:
			Card1(obj,obj.arrFrontObject , obj.events,0 , context);
			Card2(obj,obj.arrBackObject , obj.events,1 , context);
		break;
		case 1:
			Card3(obj,obj.arrFrontObject , obj.events,0 , context);
			Card4(obj,obj.arrBackObject , obj.events,1 , context);
		break;
		case 2:
			Card5(obj,obj.arrFrontObject , obj.events,0 , context);
			Card6(obj,obj.arrBackObject , obj.events,1 , context);
		break;
		case 3:
			Card7(obj,obj.arrFrontObject , obj.events,0 , context);
			Card8(obj,obj.arrBackObject , obj.events,1 , context);
		break;
		
		default:
			let text = AddText(obj.width/2 , 0,  ("Num : "+(idx+1)), 'bold', '100', '#000000', 'center');
			text.anchor.set(0.5);
			obj.arrBackObject.push(text);
			text = AddText(obj.width/2 , 0,  ("Num : "+(idx)), '', '100', '#000000', 'center');
			text.anchor.set(0.5);
			obj.arrFrontObject.push(text);
		break;
	}
	//console.log(obj);
	
	
	
	
	obj.arrBackObject.forEach((item)=>{
		obj.addChild(item);
		item.alpha = 0;
		item.scale.x = -1;
	})
	obj.arrFrontObject.forEach((item)=>{
		obj.addChild(item);
	})
}



function AddEventCard(object){
	object.sideFront = true
	
	object.onChangeScaleX = function(){
		//console.log("X changing : "+object.scale.x);
		if(object.scale.x >0){//front Side
			console.log('show front side')
			if(!object.sideFront) object.sideFront=true
			object.arrBackObject.forEach((obj)=>{
				obj.alpha = 0;
			})
			object.arrFrontObject.forEach((obj)=>{
				obj.alpha = 1;
			})
			
		}else if(object.scale.x < 0){//Back side
			console.log('show back side')
			if(object.sideFront) object.sideFront=false
			object.arrFrontObject.forEach((obj)=>{
				obj.alpha = 0;
			})
			object.arrBackObject.forEach((obj)=>{
				obj.alpha = 1;
			})
			
		}
	}
	
	
}









