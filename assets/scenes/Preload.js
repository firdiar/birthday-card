Game.Preload = function(game){
    this.game = game // keep reference to main game object
}


Game.Preload.prototype = {
    preload:function(){
        console.log(getImage("pre-loading"));
		socket.emit('event', 'Client in Loading...');
		this.loadingProgress();
		
        //tolong penamaan dimulai dengan huruf kecil
		//this.load.spritesheet('next', getImage("next.png"),512,512);
		//this.load.spritesheet('play', getImage("ButtonPlay.png"), 486 , 271);
		
		this.load.spritesheet('btn', getImage("Button.png"), 271 , 80);
		//this.load.image('Mulai' , 'asset/image/button/MulaiButton.png');
		this.load.image('Card' , getImage('Card.png'));
		this.load.image('Cake' , getImage('Cake.png'));
		this.load.image('CakeBg' , getImage('CakeBg.png'));
		this.load.image('Tali' , getImage('Tali.png'));
		this.load.image('Hope' , getImage('Hope.png'));
		this.load.spritesheet('LoveParticle' , getImage('LoveParticle.png') , 50 , 50);
		this.load.spritesheet('OpenCard' , getImage('OpenCard.png') , 160 , 155);
		this.load.spritesheet('Sparkle' , getImage('Sparkle.png') , 125 , 80);
		this.load.spritesheet('FireCaddle' , getImage('FireCaddle@2x.png') , 20 , 31);
		this.load.spritesheet('Character' , getImage('Character.png') , 360 , 350);
		this.load.spritesheet('Liontin' , getImage('Liontin.png') , 400 , 425);
		this.load.image('DialogBox' , getImage('DialogBox.png'));
		this.load.image('Password' , getImage('Password.png'));
		
		
		this.load.json('card2', getJson('Card2.json'));
		
		this.load.json('card4', getJson('Card4.json'));
		this.load.json('card7', getJson('Card7.json'));
		this.load.json('card8', getJson('Card8.json'));
		
		this.load.audio('flip', getAudio('page-flip-02.mp3'));
		this.load.audio('selamat-ulangtahun', getAudio('ultah.webm'));
		this.load.audio('happy', getAudio('happy.webm'));
		this.load.audio('hbd', getAudio('slow.webm'));
		
    },
	loadingProgress:function(){
		//loadText = AddText(gameWidth/2, gameHeight/2, '', 'bold', '60', '#ffffff', 'center'); loadText.anchor.set(.5, 1);
		this.load.onFileComplete.add(function(progress){ 
			//loadText.text = 'Loading...\n' + progress + '%' 
			//
			document.getElementById("progress").innerHTML = 'Loading...\n' + progress + '%' ;
		}, this);
		
		
		
	},
    create:function(){
		//document.getElementById("02").style.display = 'none'
		//document.getElementById("01").style.display = 'none'
		//this.state.start('mainmenu');
		document.getElementById("02").style.display = 'none'
		document.getElementById("01").style.display = 'block'
		
		this.updateTime()
		this.eventLoop = this.time.events.loop(1000,()=>this.updateTime())
		console.log(this.eventLoop);
		this.k = this.input.keyboard;
		document.getElementById("switch").disabled = false;
		
		
		
    },
	gestures :0 ,
	update:function(){

		if (this.k.isDown(65)) {
			if (this.k.isDown(68)) {
				adminMode++
				console.log(adminMode);
				if(adminMode >= 100){
					adminMode = 0;
					askAdmin();
				}
			}
		}
	},
	k : null,
	eventLoop : null,
	updateTime:function(){
		var date1 = new Date();
		var date2 = new Date(2019, 10, 9);//tlg 9-11-2019
		
		let timeDiff = date2.getTime() - date1.getTime();
		
		let remaining =  "0 Jam 0 Menit 0 Detik";
		//console.log(timeDiff < 0)
		if(timeDiff < 0){
			if(this.eventLoop!= null){
				this.eventLoop.loop = false;
			}
			document.title = "Happy Birthday Aya!"
			document.getElementById("switch").disabled = false;
			document.getElementById("status").innerHTML = "Lampu dapat dinyalakan, Selamat ulang tahun Aya...";
		}else{
			var hh = Math.floor(timeDiff / 1000 / 60 / 60);
			if(hh < 10) {
				hh = '0' + hh;
			}
			timeDiff -= hh * 1000 * 60 * 60;
			var mm = Math.floor(timeDiff / 1000 / 60);
			if(mm < 10) {
				mm = '0' + mm;
			}
			timeDiff -= mm * 1000 * 60;
			var ss = Math.floor(timeDiff / 1000);
			if(ss < 10) {
				ss = '0' + ss;
			}
			remaining =  hh + " Jam " + mm+ " Menit " + ss + " Detik";
		}
	
		document.getElementById("timer").innerHTML = remaining;
	}
}
